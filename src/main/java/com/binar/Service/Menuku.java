package com.binar.Service;

import java.util.Scanner;

public abstract class Menuku {
    public Scanner input = new Scanner(System.in);

    public int promptInput(){
        System.out.println("-----------------------------------------------------------");
        System.out.print("Masukkan pilihan : ");
        return input.nextInt();
    }
    public abstract void tampilMenu();

}
