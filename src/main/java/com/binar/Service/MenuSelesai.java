package com.binar.Service;

import java.util.InputMismatchException;

public class MenuSelesai extends Menuku{

    @Override
    public void tampilMenu(){
        MenuSelesai menuSelesai = new MenuSelesai();
        try{
            MainMenuImp main = new MainMenuImp();
            System.out.println(" " +
                    "\nMasukkan pilihan Anda : "+
                    "\n1. Kembali ke Menu Utama" +
                    "\n0. Exit");
            switch (promptInput()){
                case 0 :
                    System.out.println("Program sedang ditutup");
                    System.exit(0);
                    break;
                case 1 :
                    main.tampilMenu();
                    break;
                default:
                    this.tampilMenu();
                    break;
            }
        }catch(InputMismatchException e){
            System.err.println("Masukkan inputan Integer!\n");
        }
        menuSelesai.tampilMenu();
    }
}
